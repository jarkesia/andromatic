all:
	g++ andromatic.cpp -o andromatic
clean:
	rm andromatic
install:
	mkdir -p $(DESTDIR)/usr/bin
	cp andromatic $(DESTDIR)/usr/bin
uninstall:
	rm $(DESTDIR)/usr/bin/andromatic
