/*
   Erkki Kurenniemi's Andromatic Sequencer (1968) partiture generator
   Copyright (C) 2022  Jari Suominen

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#include <stdint.h>
#include <iostream>
#include <array>
#include <argp.h>
#include <string.h>
#include <vector>
#include <algorithm>

using namespace std;

#define COUNT 1
#define RESET 2
#define BREAK 3
#define SHIFT 4

/*
   D demo sequence:
   00001 00001
   10001 00001
   01001 00001
   11001 00001
   00101 00001
   10101 00001
   01101 00001
   11101 00001
   00010 10001
   10010 10001
   01010 10001
   11010 10001
   00110 10001
   10110 10001
   01110 10001
   11110 10001
   00001 10001
   10001 10001
   01001 10001
   11001 10001
   00101 10001
   10101 10001
   01101 10001
   11101 10001
   00010 01001
 */

//byte mode[]   =  {BREAK,  SHIFT,  SHIFT,  COUNT,  COUNT,  COUNT,  COUNT,  COUNT,  COUNT,  COUNT};   // "R sequence"
//byte mode[]     =  {COUNT,  COUNT,  COUNT,  COUNT,  SHIFT,  COUNT,  COUNT,  COUNT,  COUNT,  BREAK}; // "D sequence"
//byte mode[]     =  {BREAK,  SHIFT,  SHIFT,  SHIFT,  SHIFT,  SHIFT,  SHIFT,  SHIFT,  COUNT,  SHIFT}; // "R 2013 YT sequence"
//byte mode[]     =  {BREAK,  SHIFT,  SHIFT,  COUNT,  SHIFT,  COUNT,  SHIFT,  COUNT,  SHIFT,  COUNT}; // "MO 2012 sequence"
//std::vector<int8_t> mode;//     =  {BREAK,  SHIFT,  SHIFT,  SHIFT,  BREAK,  SHIFT,  SHIFT,  SHIFT,  BREAK,  SHIFT};
//std::array<int8_t,10> mode     =  {BREAK,  SHIFT,  SHIFT,  SHIFT,  SHIFT,  COUNT,  COUNT,  COUNT,  COUNT,  COUNT};

//boolean state[] =  {1,      0,      0,      0,      0,      0,      0,      0,      0,      0}; // "R sequence"
//boolean state[] =  {0,      0,      0,      0,      1,      0,      0,      0,      0,      1}; // "D sequence"
//boolean state[] =  {1,      0,      0,      0,      0,      0,      0,      0,      0,      0}; // "R 2013 YT sequence"
//boolean state[] =  {0,      1,      0,      0,      1,      1,      0,      0,      1,      0}; // "MO 2012 sequence"
std::vector<bool> state;// =  {0,      0,      0,      0,      1,      0,      0,      0,      1,      0};

int step = 0;

/* 0 = false, true != false */

static struct argp_option options[] = {
    { "verbose", 'v', 0, 0, "For debugging: Produce verbose output" },
    { "stepcount", 's', 0, 0, "Super debugging: Print values read from serial -- and do nothing else" },
    { 0 }
};


typedef struct _arguments {
    int stepcount, verbose;
    char* args[1];
} arguments_t;


static error_t parse_opt(int key, char *arg, struct argp_state *state)
{
    /* Get the input argument from argp_parse, which we
       know is a pointer to our arguments structure. */
    arguments_t *arguments = static_cast<arguments_t*>(state->input);
    int baud_temp;

    switch (key) {
        case 's':
            arguments->stepcount = 1;
            break;
        case 'v':
            arguments->verbose = 1;
            break;
        case ARGP_KEY_ARG:
            // Too many arguments.
            if(state->arg_num > 1)
                argp_usage(state);
            arguments->args[state->arg_num] = arg;
            break;
        case ARGP_KEY_END:
            break;

        default:
            return ARGP_ERR_UNKNOWN;
    }

    return 0;
}



void arg_set_defaults(arguments_t *arguments)
{
    arguments->stepcount = 0;
    arguments->verbose = 0;
}


void printOut(bool stepcount) {
    for (int i = 0; i < state.size(); i++) {
        cout << state[i];
    }
    if (stepcount) {
        cout << " (" << step << ")";
    }
}


const char *argp_program_version = "andromatic 0.01";
static char doc[] =
"andromatic - sequence generator replicating Kurenniemi's Andromatic synthesizer.";

static char args_doc[] = "SWITCHES";
static struct argp argp = { options, parse_opt, args_doc, doc };
arguments_t arguments;


int main(int argc, char **argv) {

    arg_set_defaults(&arguments);
    argp_parse(&argp, argc, argv, 0, 0, &arguments);

    std::vector<int8_t> mode;
    int steps = strlen(arguments.args[0]);

    for (int i = 0; i < steps; i++) {
        switch (*(arguments.args[0]+i)) {
            case 'C':
                mode.push_back(COUNT);
                break;
            case 'S':
                mode.push_back(SHIFT);
                break;
            case 'B':
                mode.push_back(BREAK);
                break;
            case 'R':
                if (i == 0) {
                    mode.push_back(RESET);
                }
                else {
                    cout << "ERROR: RESET only allowed on step #1." << endl;
                    return 0;
                }
                break;
            default:
                cout << "ERROR: Illegal mode: " << *(arguments.args[0]+i) << " ." << endl;
                return 0;
                break;
        }
    }


    for (int i = 0; i < steps; i++) {
        switch(mode[i]) {
            case COUNT:
                cout << "C";
                state.push_back(0);
                break;
            case SHIFT:
                cout << "S";
                state.push_back(0);
                break;
            case BREAK:
                cout << "B";
                state.push_back(1);
                break;
            case RESET:
                cout << "R";
                state.push_back(1);
                break;
        }
    }
    if (arguments.verbose) {
        cout << " [" << steps << " stages]";
    }
    cout << endl;

    printOut(arguments.stepcount);
    cout << endl;

    std::vector<vector<bool>> sequence;

    sequence.push_back(state);

    while (true) {

        bool shift = true;
        bool df = true;         // Data Forward DF
        bool prevState = state[0];
        bool tmp;

        for (int i = 0; i < steps; i++) {

            /*
               Main logic of Andromatic sequencer is contained inside this for-loop.
               It is based on original schematics and confirmed with video footage
               of the original hardware shot betweem 2004-2013.
             */

            switch (mode[i]) {
                case COUNT:
                    prevState = state[i];
                    if (df) {
                        state[i] = !state[i];
                    }
                    shift = df;
                    break;

                case SHIFT:
                    tmp = state[i];
                    if (shift) {
                        state[i] = prevState;
                    }
                    prevState = tmp;
                    break;

                case BREAK:
                    prevState = state[i];
                    if (df) {
                        int8_t b = i + 1;
                        while ((b < steps) && (mode[b] == SHIFT)) {
                            b++;
                        }
                        state[i] = state[b - 1];
                    }
                    shift = df;
                    break;

                case RESET:
                    state[i] = 1;
                    shift = false;
                    df = false;
                    /* RESET should also trigger initialization routine on all stages. */
                    break;

            }

            df = (prevState != state[i]) && (state[i] == 0);  // If falling edge then will trig df line.

        }

        step++;
        printOut(arguments.stepcount);

        auto l = std::find(sequence.begin(), sequence.end(), state);

        if ( l != sequence.end() ) {

            cout << " => (" << l - sequence.begin() << ")" << endl;
                cout << "Sequence length: " << sequence.size() - (l - sequence.begin()) << " steps." << endl;
            break;
        }

        sequence.push_back(state);

        cout << endl;

    }

    return 0;

}



void setMode(int s, int m) {
    //if (mode[s] == m)
    //				return;

    //if ((m == BREAK) || (mode[s] == BREAK)) {

    /* Because of reasons, in the original hardware,
       in BREAK mode, bits are inpterpreted as inverted.
       We are not doing it here, instead simply flip them
       when mode is changed.

       Note that RESET is also in inverted mode so
       this function will not work properly.
     */

    //			state[s] = !state[s];
    //	}

    //mode[s] = m;

}


